//
//  Country.swift
//  Shared
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RealmSwift

public class DbCountry: Object{
    @objc dynamic var ID: Int = -1
    @objc dynamic var intCode: Int = -1
    @objc dynamic var name: String = ""
    
    convenience init(ID: Int, intCode: Int, name: String = "") {
        self.init()
        self.ID = ID
        self.intCode = intCode
        self.name = name
    }
}

public struct Country{
    public var ID: Int = -1
    public var intCode: Int = -1
    public var name: String = ""
    
    public init(ID: Int, name: String, intCode: Int) {
        self.ID = ID
        self.name = name
        self.intCode = intCode
    }
}
