//
//  Player.swift
//  Shared
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RealmSwift

public class DbPlayer: Object{
    @objc dynamic var uniqueId: String = ""
    @objc dynamic var ID: Int = -1
    @objc dynamic var dateOfBirth: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var countryId: Int = -1
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var placeOfBirth: String = ""
    
    override public class func primaryKey() -> String? { return "ID" }
    
    convenience init(uniqueID: String, ID: Int, dateOfBirth: Date, countryId: Int,firstName: String, lastName: String, placeOfBirth: String) {
        self.init()
        self.uniqueId = uniqueID
        self.ID = ID
        self.dateOfBirth = dateOfBirth
        self.countryId = countryId
        self.firstName = firstName
        self.lastName = lastName
        self.placeOfBirth = placeOfBirth
    }
}

public struct Player{
    public var uniqueId: String = ""
    public var ID: Int = -1
    public var dateOfBirth: Date = Date(timeIntervalSince1970: 1)
    public var countryId: Int = -1
    public var firstName: String = ""
    public var lastName: String = ""
    public var placeOfBirth: String = ""
    
    public init(uniqueId: String, ID: Int,dateOfBirth: Date,countryId: Int,firstName: String,lastName: String,placeOfBirth: String) {
        self.uniqueId = uniqueId
        self.ID = ID
        self.dateOfBirth = dateOfBirth
        self.countryId = countryId
        self.firstName = firstName
        self.lastName = lastName
        self.placeOfBirth = placeOfBirth
    }
}
