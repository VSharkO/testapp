//
//  PlayerRegistration.swift
//  Shared
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RealmSwift

public class DbPlayerRegistration: Object{
    @objc dynamic var ID: Int = -1
    @objc dynamic var dateFrom: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var dateTo: Date = Date(timeIntervalSince1970: 1)
    @objc dynamic var clubId: Int = -1
    @objc dynamic var playerId: Int = -1
    
    convenience init(ID: Int, dateFrom: Date, dateTo: Date, clubId: Int, playerId: Int) {
        self.init()
        self.ID = ID
        self.dateFrom = dateFrom
        self.dateTo = dateTo
        self.clubId = clubId
        self.playerId = playerId
    }
}

public struct PlayerRegistration{
    public var ID: Int = -1
    public var dateFrom: Date = Date(timeIntervalSince1970: 1)
    public var dateTo: Date = Date(timeIntervalSince1970: 1)
    public var clubId: Int = -1
    public var playerId: Int = -1
    
    public init(ID: Int,dateFrom: Date,dateTo: Date,clubId: Int,playerId: Int) {
        self.ID = ID
        self.dateFrom = dateFrom
        self.dateTo = dateTo
        self.clubId = clubId
        self.playerId = playerId
    }
}
