//
//  Club.swift
//  Shared
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RealmSwift

public class DbClub: Object{
    @objc dynamic var ID: Int = -1
    @objc dynamic var name: String = ""
    @objc dynamic var type: String = ""
    
    convenience init(ID: Int, name: String, type: String) {
        self.init()
        self.ID = ID
        self.name = name
        self.type = type
    }
}

public struct Club{
    public var ID: Int = -1
    public var name: String = ""
    public var type: String = ""
    
    public init(ID: Int, name: String, type: String) {
        self.ID = ID
        self.name = name
        self.type = type
    }
}
