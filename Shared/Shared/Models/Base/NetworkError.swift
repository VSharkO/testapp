//
//  NetworkError.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation

public enum NetworkError: Error {
    case parseFailed
    case empty
    case generalError
}
