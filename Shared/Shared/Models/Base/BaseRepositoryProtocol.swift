//
//  BaseRepositoryProtocol.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import Alamofire
public protocol BaseRepositoryProtocol {
    
}
/**
 Extension that enables you to create global code for Repositories. Place here code for common actions - like creating API key param.
 
 Example of method:
 
 internal func createRequestParams() -> Parameters {
 var params = Parameters()
 params["api_token"] = RestEndpoints.API_TOKEN
 
 return params
 }
 */
extension BaseRepositoryProtocol {
    
}
