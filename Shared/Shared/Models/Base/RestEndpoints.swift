//
//  RestEndpoints.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
/**
 Enum that stores all API Endpoints.
 
 Example of proper written EndpointEnum:
 
 enum RestEndpoints{
 static let ENDPOINT: String = Bundle.main.infoDictionary!["API_BASE_URL_ENDPOINT"] as! String
 case index
 case article(articleId: Int)
 func endpoint() -> String {
 
 switch self {
 case .index:
 return RestEndpoints.ENDPOINT+"index"
 
 case .article(let articleId):
 return RestEndpoints.ENDPOINT+"clanak/\(articleId)"
 }
 }
 
 */
public enum RestEndpoints{
    static let ENDPOINT: String = Bundle.main.infoDictionary!["API_BASE_URL_ENDPOINT"] as! String
    
    func endpoint() -> String {
        
        switch self {
            
        }
    }
}
