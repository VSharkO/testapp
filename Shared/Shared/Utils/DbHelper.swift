//
//  DbHelper.swift
//  Shared
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
public class DbHelper{
    
    public init() {}
    let realm = try? Realm()
    
    public func savePlayerToDb(player: Player){
        guard let realm = realm else{return}
        var isContained = false
        let playerForDb = DbPlayer(uniqueID: player.uniqueId, ID: player.ID, dateOfBirth: player.dateOfBirth, countryId: player.countryId, firstName: player.firstName, lastName: player.lastName, placeOfBirth: player.placeOfBirth)
        let dbPlayers = realm.objects(DbPlayer.self)
        if !dbPlayers.filter({$0.uniqueId == playerForDb.uniqueId}).isEmpty{
            isContained = true
        }
        if !isContained{
            try? realm.write {
                realm.add(playerForDb)
            }
        }
    }
    
    public func getPlayersFromDb() -> Observable<[Player]>{
        guard let realm = realm else{return Observable.just([])}
        let dbPlayers = realm.objects(DbPlayer.self)
        var players: [Player] = []
        dbPlayers.forEach{players.append(Player(uniqueId: $0.uniqueId, ID: $0.ID, dateOfBirth: $0.dateOfBirth, countryId: $0.countryId, firstName: $0.firstName, lastName: $0.lastName, placeOfBirth: $0.placeOfBirth))}
        return Observable.just(players)
    }
    
    public func saveRegistrationToDb(registration: PlayerRegistration){
        guard let realm = realm else{return}
        var isContained = false
        let registrationForDb = DbPlayerRegistration(ID: registration.ID, dateFrom: registration.dateFrom, dateTo: registration.dateTo, clubId: registration.clubId, playerId: registration.playerId)
        let dbRegistrations = realm.objects(DbPlayerRegistration.self)
        if !dbRegistrations.filter({$0.ID == registrationForDb.ID}).isEmpty{
            isContained = true
        }
        if !isContained{
            try? realm.write {
                realm.add(registrationForDb)
            }
        }
    }
    
    public func getRegistrationsFromDb() -> Observable<[PlayerRegistration]>{
        guard let realm = realm else{return Observable.just([])}
        let dbPlayers = realm.objects(DbPlayerRegistration.self)
        var registrations: [PlayerRegistration] = []
        dbPlayers.forEach{registrations.append(PlayerRegistration(ID: $0.ID, dateFrom: $0.dateFrom, dateTo: $0.dateTo, clubId: $0.clubId, playerId: $0.playerId))}
        return Observable.just(registrations)
    }
    
    public func getClubsFromDb() -> Observable<[Club]>{
        guard let realm = realm else{return Observable.just([])}
        let dbClubs = realm.objects(DbClub.self)
        var clubs: [Club] = []
        dbClubs.forEach{clubs.append(Club.init(ID: $0.ID, name: $0.name, type: $0.type))}
        return Observable.just(clubs)
    }
    
    public func getCountriesFromDb() -> Observable<[Country]>{
        guard let realm = realm else{return Observable.just([])}
        let dbCountries = realm.objects(DbCountry.self)
        var countries: [Country] = []
        dbCountries.forEach{countries.append(Country.init(ID: $0.ID, name: $0.name, intCode: $0.intCode))}
        return Observable.just(countries)
    }

}
