//
//  Theme.swift
//  Shared
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import BaseKit
import BaseUIKit
public class Theme {
    private init(){
//        print("fonts: \(UIFont.familyNames)")
        
    }
    public static let shared: Theme = Theme()
    
    func setNavigationBarColor(_ color: UIColor){
        UINavigationBar.appearance().barTintColor = color
    }
    
    //setup app theme.
    public func applyTheme() {
        UIApplication.shared.statusBarStyle = .lightContent
        LoaderHUD.applyColorToSpinner(color: .primaryColor)
        let navigationBar = UINavigationBar.appearance()
        navigationBar.isTranslucent = false
        navigationBar.tintColor = UIColor.white
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        setNavigationBarColor(UIColor.primaryColor)
    }
    
    /**
     Defines customation styles for the app content.
     Example:
     struct Styles {
     private init(){}
     struct Text {
     struct Regular {
     static let small = ColorStyle(font: Fonts.roboto.smallSize)
     static let normal = ColorStyle(font: Fonts.roboto.normalSize)
     static let big = ColorStyle(font: Fonts.roboto.bigSize)
     }
     struct Bold {
     static let small = ColorStyle(font: Fonts.robotoBold.smallSize)
     static let normal = ColorStyle(font: Fonts.robotoBold.normalSize)
     static let big = ColorStyle(font: Fonts.robotoBold.bigSize)
     static let extraBig = ColorStyle(font: Fonts.robotoBold.extraSize)
     
     }
     }
     */
    public  struct Styles {
        private init(){}
        
        public struct Text {
            public struct Regular {
                public static let small = ColorStyle(font: Fonts.roboto.smallSize)
                public static let normal = ColorStyle(font: Fonts.roboto.normalSize)
            }
        }
    }
    
    /**
     Exmaple:
     private struct Fonts{
     private init(){}
     static let roboto = FontStyle(fontName: "Roboto-Regular")
     static let robotoBold = FontStyle(fontName: "Roboto-Bold")
     }
     */
    private struct Fonts{
        //Define your fonts
        private init(){}
        static let roboto = FontStyle(fontName: "Roboto-BlackItalic")
    }
    /**
     Example:
     
     internal struct ColorStyle {
     fileprivate  let font: UIFont
     let defaultColor: Style
     let blackColor: Style
     
     
     fileprivate init(font: UIFont){
     self.font = font
     defaultColor = Style(textColor: UIColor.normalTextColor,font: font)
     blackColor = Style(textColor: UIColor.blackTextColor, font: font)
     }
     
     }
     */
    public struct ColorStyle {
        fileprivate  let font: UIFont
        //Define your colors!
        public let blackColor: Style
        
        public init(font: UIFont){
            self.font = font
            blackColor = Style(textColor: .black, font: font)
        }
    }
    /**
     Example :
     internal struct FontStyle {
     let smallSize: UIFont
     
     fileprivate init(fontName: String){
     smallSize = UIFont(name: fontName, size: 12)!
     }
     /**
     Initialze with default system italic font.
     */
     fileprivate init(){
     smallSize = UIFont.italicSystemFont(ofSize: 12)
     
     }
     }
     */
    public struct FontStyle {
        let smallSize: UIFont
        let normalSize: UIFont
        public init(fontName: String){
            smallSize = UIFont(name: fontName, size: 12)!
            normalSize = UIFont(name: fontName, size: 14)!
        }
    }
}
