//
//  NSObject+Extension.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public extension NSObject {
    func printDeinit() {
        print("deinit:", String(describing: self))
    }
}
