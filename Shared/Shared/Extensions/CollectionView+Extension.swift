//
//  CollectionView+Extension.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import UIKit
public extension UICollectionView {
    
    // dequeueing
    func dequeue<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Can't dequeue cell with identifier: \(T.identifier)")
        }
        return cell
    }
}
extension UICollectionReusableView: Identifiable{
    
}
