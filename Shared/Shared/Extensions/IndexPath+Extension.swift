//
//  IndexPath+Extension.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public extension IndexPath {
    static func  createIndexPaths(section: Int, rows: [Int]) -> [IndexPath]{
        return rows.map({ (index) -> IndexPath in
            return IndexPath(row: index, section: section)
        })
    }
}
