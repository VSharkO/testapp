//
//  Identifiable.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public protocol Identifiable{
    
}
public extension Identifiable {
    static var identifier: String {
        return String(describing: self)
    }
}

