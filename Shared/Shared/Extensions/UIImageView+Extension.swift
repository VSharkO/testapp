//
//  UIImageView+Extension.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import Kingfisher
public extension UIImageView {
    func load(_ urlString:String, placeholder: UIImage?){
        
        if let url = URL(string: urlString){
            self.kf.setImage(with: ImageResource(downloadURL: url), placeholder: placeholder, options: nil, progressBlock: nil) { (result) in
                
                if let error = result.error{
                    debugPrint("image download failed: \(String(describing: error))\n url: \(String(describing: url))")
                }
            }
        }
    }
}
