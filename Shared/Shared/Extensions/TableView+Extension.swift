//
//  TableView+Extension.swift
//  Shared
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import UIKit
public extension UITableView {
    
    // dequeueing
    func dequeue<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        
        guard let cell = self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Can't dequeue cell with identifier: \(T.identifier)")
        }
        
        return cell
    }
    
    func dequeueHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: T.identifier) as? T else {
            fatalError("Can't dequeue view with identifier: \(T.identifier)")
        }
        
        return view
    }
}
extension UITableViewHeaderFooterView: Identifiable{
    
}
extension UITableViewCell: Identifiable{
    
}
