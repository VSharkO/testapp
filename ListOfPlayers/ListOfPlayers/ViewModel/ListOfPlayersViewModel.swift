//
//  ListOfPlayersViewModel.swift
//  ListOfPlayers
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import BaseKit
import RxSwift

public class ListOfPlayersViewModel: ViewModelType{
    
    
    public struct Dependecies{}
    
    public struct Input{}
    
    public struct Output{}
    
    public func transform(input: ListOfPlayersViewModel.Input) -> ListOfPlayersViewModel.Output {
        return Output()
    }
}

//MARK: Data manipulation
public extension ListOfPlayersViewModel{
    
}
