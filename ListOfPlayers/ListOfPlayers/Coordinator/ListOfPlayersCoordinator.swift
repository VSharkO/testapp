//
//  ListOfPlayersCoordinator.swift
//  ListOfPlayers
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import Shared
import BaseKit

public class ListOfPlayersCoordinator: NSObject, Coordinator, CoordinatorDelegate{
    
    public weak var parent: ParentCoordinatorDelegate?
    public var childCoordinators: [Coordinator] = []
    public var presenter: UINavigationController
    let viewModel: ListOfPlayersViewModel
    let controller: ListOfPlayersViewController
    
    public init(presenter: UINavigationController){
        self.presenter = presenter
        viewModel = ListOfPlayersViewModel()
        controller = ListOfPlayersViewController(viewModel: viewModel)
    }
    
    deinit {
        printDeinit()
    }
    
    public func start() {
        controller.coordinatorDelegate = self
        presenter.pushViewController(controller, animated: true)
    }
    
    public func viewControllerHasFinished() {
        parent?.childHasFinished(coordinator: self)
    }
}

