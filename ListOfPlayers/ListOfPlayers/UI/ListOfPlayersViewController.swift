//
//  ListOfPlayersViewController.swift
//  ListOfPlayers
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import Shared
import BaseUIKit
import BaseKit
import RxSwift

public class ListOfPlayersViewController: UIViewController{
    public weak var coordinatorDelegate: CoordinatorDelegate?
    public var viewModel: ListOfPlayersViewModel

    override public func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("Displaying \(ListOfPlayersViewController.self)")
    }
    
    init(viewModel: ListOfPlayersViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "\(ListOfPlayersViewController.self)", bundle: Bundle(for: ListOfPlayersViewController.self))
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent{
            coordinatorDelegate?.viewControllerHasFinished()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        printDeinit()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
