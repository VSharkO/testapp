//
//  HomeViewController.swift
//  Home
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import Shared
import SnapKit
import BaseUIKit
import Rswift

class HomeViewController: UIViewController {
    public weak var coordinatorDelegate: HomeCoordinatorDelegate?
    let buttonNewPlayer: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(R.string.localizable.title_new_player(), for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    let buttonPlayersList: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(R.string.localizable.title_list_players(), for: .normal)
        button.backgroundColor = .red
        return button
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.applyStyle(Theme.Styles.Text.Regular.normal.blackColor)
        label.textAlignment = .center
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setupTargets()
    }
    
    func setupViews(){
        self.view.addSubviews(buttonNewPlayer,buttonPlayersList)
        setupConstraints()
    }
    
    func setupConstraints(){
        buttonNewPlayer.snp.makeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(view).inset(13)
        }
        buttonPlayersList.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(view).inset(13)
            maker.top.equalTo(buttonNewPlayer.snp_bottomMargin).offset(13)
        }
    }
    
    func setupTargets(){
        buttonNewPlayer.addTarget(self, action: #selector(openNewPlayer), for: .touchUpInside)
        buttonPlayersList.addTarget(self, action: #selector(openPlayersList), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func openNewPlayer(){
        coordinatorDelegate?.openNewPlayer()
    }
    
    @objc func openPlayersList(){
        coordinatorDelegate?.openPlayersList()
    }
    
}
