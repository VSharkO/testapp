//
//  HomeCoordinatorDelegate.swift
//  Home
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation

protocol HomeCoordinatorDelegate: class {
    func openNewPlayer()
    func openPlayersList()
}
