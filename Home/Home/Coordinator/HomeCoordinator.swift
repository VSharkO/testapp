//
//  HomeCoordinator.swift
//  Home
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import BaseKit
import PlayerForm
import ListOfPlayers

public class HomeCoordinator: Coordinator, HomeCoordinatorDelegate, ParentCoordinatorDelegate{
    public var childCoordinators: [Coordinator] = []
    public var presenter: UINavigationController
    let controller: HomeViewController
    
    public  init (presenter: UINavigationController){
        self.presenter = presenter
        controller = HomeViewController()
        //Add ViewModel initialization.
    }
    
    public func start() {
        controller.coordinatorDelegate = self
        presenter.pushViewController(controller, animated: true)
    }
    
    public func childHasFinished(coordinator: Coordinator) {
        removeChildCoordinator(childCoordinator: coordinator)
    }

    func openNewPlayer() {
        let coordinator = PlayerFormCoordinator(presenter: presenter)
        childCoordinators.append(coordinator)
        coordinator.parent = self
        coordinator.start()
    }
    
    func openPlayersList() {
        let coordinator = ListOfPlayersCoordinator(presenter: presenter)
        childCoordinators.append(coordinator)
        coordinator.parent = self
        coordinator.start()
    }
    
}
