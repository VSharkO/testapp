//
//  HomeViewModel.swift
//  Home
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import BaseKit

class HomeViewModel: ViewModelType{
    
    typealias Dependecies = Void
    
    typealias Input = Void
    
    typealias Output = Void
    
    func transform(input: Void) -> Void {
        
    }
}
