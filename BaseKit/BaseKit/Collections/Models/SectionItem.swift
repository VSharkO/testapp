//
//  SectionItem.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
@available(*, deprecated: 11.0, obsoleted: 13.0, renamed: "SectionItem", message: "Migrate to SectionItem, this will be removed in next version.")
public struct TableSectionItem<SectionType,ItemType, ItemData>: Collection{
    public typealias Index = Int
    // Required nested types, that tell Swift what our collection contains
    public typealias Element = TableItem<ItemType,ItemData>
    public var startIndex: Index {return items.startIndex}
    public var endIndex: Index {return items.endIndex}
    public let type: SectionType
    public let sectionTitle: String
    public let footerTitle: String
    @available(*,deprecated, message: "Table section now conforms to Collection. Use subscript to access data.")
    private var items: [Element]
    
    
    public init (type: SectionType, sectionTitle: String, footerTitle: String,  items: [TableItem<ItemType,ItemData>]){
        self.type = type
        self.sectionTitle = sectionTitle
        self.footerTitle = footerTitle
        self.items = items
    }
    
    public subscript(position: Int) -> Element {
        return items[position]
    }
    public func index(after i: Int) -> Index {
        return items.index(after:i)
    }
}

extension TableSectionItem: Equatable where SectionType: Equatable, ItemType: Equatable, ItemData: Equatable {}


public struct SectionItem<SectionType,ItemType, ItemData>: Collection{
    public typealias Index = Int
    // Required nested types, that tell Swift what our collection contains
    public typealias Element = RowItem<ItemType,ItemData>
    public var startIndex: Index {return items.startIndex}
    public var endIndex: Index {return items.endIndex}
    public let type: SectionType
    public let sectionTitle: String
    public let footerTitle: String
    private var items: [Element]
    
    
    public init (type: SectionType, sectionTitle: String, footerTitle: String,  items: [Element]){
        self.type = type
        self.sectionTitle = sectionTitle
        self.footerTitle = footerTitle
        self.items = items
    }
    
    public subscript(position: Int) -> Element {
        return items[position]
    }
    public func index(after i: Int) -> Index {
        return items.index(after:i)
    }
}

extension SectionItem: Equatable where SectionType: Equatable, ItemType: Equatable, ItemData: Equatable {}
