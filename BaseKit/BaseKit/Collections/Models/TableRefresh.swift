//
//  TableRefresh.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation

public enum TableRefresh {
    case complete
    case reloadRows(indexPaths: [IndexPath])
    case updateRows(indexPaths: [IndexPath])
    case section(section: Int, withAnimation: UITableView.RowAnimation )
    case addRows(indexPaths: [IndexPath], withAnimation: UITableView.RowAnimation)
    case removeRows(indexPaths: [IndexPath], withAnimation: UITableView.RowAnimation)
    case addSection(withIndex:Int)
    case removeSection(withIndex:Int)
    case dontRefresh
    case multipleActions(removeIndexes:[IndexPath], addIndexes:[IndexPath],modifiedIndexes:[IndexPath])
}

extension TableRefresh: Equatable {
    public static func == (lhs: TableRefresh, rhs: TableRefresh) -> Bool {
        switch (lhs, rhs) {
        case (.complete, .complete):
            return true
        case (.reloadRows, .reloadRows):
            return true
        case (.updateRows, .updateRows):
            return true
        case (.section, .section):
            return true
        case (.addRows, .addRows):
            return true
        case (.removeRows, .removeRows):
            return true
        case (.addSection, .addSection):
            return true
        case (.removeSection, .removeSection):
            return true
        case (.dontRefresh, .dontRefresh):
            return true
        case (.multipleActions, .multipleActions):
            return true
        default:
            return false
        }
    }
}
