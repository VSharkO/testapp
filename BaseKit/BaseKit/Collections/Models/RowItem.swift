//
//  RowItem.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
@available(*, deprecated: 11.0, obsoleted: 13.0, renamed: "RowItem", message: "Migrate to RowItem, this will be removed in next version.")
public struct TableItem <ItemType, DataType> {
    public var type: ItemType
    public var data: DataType
    
    public init(type: ItemType, data: DataType){
        self.type = type
        self.data = data
    }
}

extension TableItem: Equatable where ItemType: Equatable, DataType: Equatable {}

public struct RowItem <ItemType, DataType> {
    public var type: ItemType
    public var data: DataType
    
    public init(type: ItemType, data: DataType){
        self.type = type
        self.data = data
    }
}

extension RowItem: Equatable where ItemType: Equatable, DataType: Equatable {}
