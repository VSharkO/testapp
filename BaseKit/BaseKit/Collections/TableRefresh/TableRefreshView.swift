//
//  TableRefreshView.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RxSwift

public protocol TableRefreshView {
    associatedtype ItemRefreshable
    var itemRefreshable: ItemRefreshable { get }
    var disposeBag: DisposeBag {get}
    
    func initializeRefreshDriver(refreshObservable: Observable<TableRefresh>)
    func reloadTable(itemRefresh: TableRefresh, itemRefreshable: ItemRefreshable)
}
/**
 Extension that handles cell refreshes
 
 Setup :
 In TableView controller call initializeRefreshDriver(refreshObservable: Observable<TableRefresh>) where  refreshObservable is refreshObserverable from TableRefreshViewModelProcotol.
 
 In class that implements TableRefreshViewModelProcotol emit one of the TableRefresh events.
 Check reloadTable method for more info.
 If necessery, it's it possible to override reloadTable method and implement custom reload scenarios.
 */
public extension TableRefreshView where Self: AnyObject{
    public   func initializeRefreshDriver(refreshObservable: Observable<TableRefresh>){
        refreshObservable
            .asDriver(onErrorJustReturn: TableRefresh.dontRefresh)
            .do(onNext: { [unowned self] (itemRefresh) in
                self.reloadTable(itemRefresh: itemRefresh, itemRefreshable: self.itemRefreshable)
            })
            .drive()
            .disposed(by: disposeBag)
    }
}

public extension TableRefreshView where ItemRefreshable: UICollectionView{
    public func reloadTable(itemRefresh: TableRefresh, itemRefreshable: ItemRefreshable){
        switch(itemRefresh){
        case .complete:
            debugPrint("reloading collection for \(self)")
            itemRefreshable.reloadData()
        case .addSection(let index):
            let index = IndexSet(integer: index)
            itemRefreshable.insertSections(index)
            debugPrint("add collection section with index : \(index) for \(self)")
        case .addRows(let indexPaths, _):
            let lastScrollOffset = itemRefreshable.contentOffset
            itemRefreshable.performBatchUpdates({
                itemRefreshable.insertItems(at: indexPaths)
            }) { _ in
                itemRefreshable.setContentOffset(lastScrollOffset, animated: false)
            }
            debugPrint("insert collection items with index : \(indexPaths) for \(self)")
        case .removeRows(let indexPaths, _):
            itemRefreshable.deleteItems(at: indexPaths)
            debugPrint("delete collection items with index : \(indexPaths) for \(self)")
        case .removeSection(let index):
            let index = IndexSet(integer: index)
            itemRefreshable.deleteSections(index)
            debugPrint("remove collection section with index : \(index) for \(self)")
        case .section(let index, _):
            let index = IndexSet(integer: index)
            itemRefreshable.reloadSections(index)
            debugPrint("reloading collection section with index : \(index) for \(self)")
        case .reloadRows(let indexPaths):
            debugPrint("reloading collection items with index : \(indexPaths) for \(self)")
            itemRefreshable.reloadItems(at: indexPaths)
        case .multipleActions(let removeIndexes, let addIndexes, let modifiedIndexes):
            let lastScrollOffset = itemRefreshable.contentOffset
            itemRefreshable.performBatchUpdates({
                itemRefreshable.deleteItems(at: removeIndexes)
                itemRefreshable.reloadItems(at: modifiedIndexes)
                itemRefreshable.insertItems(at: addIndexes)
            }) { _ in
                itemRefreshable.setContentOffset(lastScrollOffset, animated: false)
            }
            debugPrint("multiple operations on collection without animation for \(self)" )
        default:
            return
        }
    }
}

public extension TableRefreshView where ItemRefreshable: UITableView{
    public func reloadTable(itemRefresh: TableRefresh, itemRefreshable: ItemRefreshable){
        switch(itemRefresh){
        case .complete:
            debugPrint("reloading table for \(self)")
            itemRefreshable.reloadData()
        case .addSection(let index):
            let index = IndexSet(integer: index)
            itemRefreshable.insertSections(index, with: .automatic)
            debugPrint("add table section with index : \(index) for \(self)")
        case .addRows(let indexPaths, let animation):
            if(animation == .none){
                let lastScrollOffset = itemRefreshable.contentOffset
                UIView.setAnimationsEnabled(false)
                itemRefreshable.beginUpdates()
                itemRefreshable.insertRows(at: indexPaths, with: .none)
                itemRefreshable.endUpdates()
                itemRefreshable.layer.removeAllAnimations()
                itemRefreshable.setContentOffset(lastScrollOffset, animated: false)
                UIView.setAnimationsEnabled(true)
                debugPrint("insert table rows with index : \(indexPaths) without animation for \(self)" )
            } else {
                itemRefreshable.insertRows(at: indexPaths, with: animation)
                debugPrint("insert table rows with index : \(indexPaths) for \(self)")
            }
        case .removeRows(let indexPaths, let animation):
            itemRefreshable.deleteRows(at: indexPaths, with: animation)
            debugPrint("delete table rows with index : \(indexPaths) for \(self)")
        case .removeSection(let index):
            let index = IndexSet(integer: index)
            itemRefreshable.deleteSections(index, with: .automatic)
            debugPrint("remove table section with index : \(index) for \(self)")
        case .section(let index, let animation):
            let index = IndexSet(integer: index)
            itemRefreshable.reloadSections(index, with: animation)
            debugPrint("reloading table section with index : \(index) for \(self)")
        case .reloadRows(let indexPaths):
            debugPrint("reloading table rows with index : \(indexPaths) for \(self)")
            itemRefreshable.reloadRows(at: indexPaths, with: .none)
        case .multipleActions(let removeIndexes, let addIndexes, let modifiedIndexes):
            let lastScrollOffset = itemRefreshable.contentOffset
            UIView.setAnimationsEnabled(false)
            itemRefreshable.beginUpdates()
            itemRefreshable.deleteRows(at: removeIndexes, with: .none)
            itemRefreshable.reloadRows(at: modifiedIndexes, with: .none)
            itemRefreshable.insertRows(at: addIndexes, with: .none)
            itemRefreshable.endUpdates()
            
            itemRefreshable.layer.removeAllAnimations()
            itemRefreshable.setContentOffset(lastScrollOffset, animated: false)
            UIView.setAnimationsEnabled(true)
            debugPrint("multiple operations on table without animation for \(self)" )
        default:
            return
        }
    }
}
