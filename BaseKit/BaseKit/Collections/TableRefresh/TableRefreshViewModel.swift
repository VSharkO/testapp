//
//  TableRefreshViewModel.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import RxSwift
public protocol TableRefreshViewModelProtocol {
    var refreshView: PublishSubject<TableRefresh> {get}
}
