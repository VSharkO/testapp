//
//  ViewModelType.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public protocol ViewModelType{
    associatedtype Dependecies
    associatedtype Input
    associatedtype Output
    
    
    func transform(input: Input) -> Output
}

