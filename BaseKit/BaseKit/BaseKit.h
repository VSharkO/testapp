//
//  BaseKit.h
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BaseKit.
FOUNDATION_EXPORT double BaseKitVersionNumber;

//! Project version string for BaseKit.
FOUNDATION_EXPORT const unsigned char BaseKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BaseKit/PublicHeader.h>


