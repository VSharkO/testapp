//
//  CoordinatorDelegate.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public protocol CoordinatorDelegate: class {
    func viewControllerHasFinished()
}

public protocol ParentCoordinatorDelegate: class {
    func childHasFinished(coordinator: Coordinator)
}

