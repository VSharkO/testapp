//
//  LoaderViewModel.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import RxSwift
public protocol LoaderViewModelProtocol {
    var loaderPublisher: PublishSubject<Bool> {get}
}
