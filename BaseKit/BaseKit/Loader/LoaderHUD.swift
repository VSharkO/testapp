//
//  LoaderHud.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import UIKit
public class LoaderHUD {
    public static let LOADER_SIZE = CGFloat(44)
    private static let loaderView : LoaderOverlayView = {
        return LoaderOverlayView()
    }()
    
    public static func applyColorToSpinner(color: UIColor){
        loaderView.spinnerView.spinnerColor = color.cgColor
    }
    static func showLoader(viewController: UIViewController){
        
        if let superview = loaderView.superview {
            if( superview == viewController.view) {
                return  //noting to do here as we are already showing loader.
            }else{
                loaderView.removeFromSuperview()
            }
            
        }
        viewController.view.addSubview(loaderView)
        loaderView.spinnerView.animate()
        
    }
    static func dismissLoader(viewController: UIViewController){
        loaderView.spinnerView.stopAnimation()
        loaderView.removeFromSuperview()
    }
}
