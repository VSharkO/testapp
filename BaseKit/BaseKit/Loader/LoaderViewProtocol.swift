//
//  LoaderViewProtocol.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
public protocol LoaderViewProtocol: class{
    var disposeBag: DisposeBag {get}
    func showLoader()
    func dismissLoader()
}
public extension  LoaderViewProtocol where Self: UIViewController {
    func showLoader(){
        LoaderHUD.showLoader(viewController: self)
    }
    
    func dismissLoader(){
        LoaderHUD.dismissLoader(viewController: self)
    }
    
    func initializeLoaderObserver(_ loaderObservable: Observable<Bool>){
        loaderObservable
            .asDriver(onErrorJustReturn: false)
            .do(onNext: { [unowned self] (showLoader) in
                if(showLoader){
                    self.showLoader()
                }else {
                    self.dismissLoader()
                }
            })
            .drive()
            .disposed(by: disposeBag)
    }
}
