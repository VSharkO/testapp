//
//  LoaderOverlayView.swift
//  BaseKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
public class LoaderOverlayView : UIView {
    let spinnerView: SpinnerView!
    
    override init(frame: CGRect) {
        let realFrame = UIScreen.main.bounds
        spinnerView = SpinnerView(frame: CGRect(origin: realFrame.origin, size: CGSize(width: LoaderHUD.LOADER_SIZE, height: LoaderHUD.LOADER_SIZE)))
        super.init(frame: realFrame)
        backgroundColor = .clear
        spinnerView.center = center
        addSubview(spinnerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
