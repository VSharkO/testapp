//
//  DataWrapper.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public struct DataWrapper<T> {
    public let data: T?
    public let error: Error?
    public let page: Int
    
    public init (data: T?, error: Error?, page: Int){
        self.data = data
        self.error = error
        self.page = page
    }
    
}
