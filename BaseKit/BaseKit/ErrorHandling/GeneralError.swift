//
//  GeneralError.swift
//  BaseKit
//
//  Created by Valentin Šarić on 23/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public enum GeneralError: Error {
    case internetUnavailable
    case serverError
    case none
}
