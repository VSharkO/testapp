//
//  ButtonCell.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import SnapKit
import Shared

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    
    var buttonIsClicked: ((FormCellType) -> Void)?
    var type: FormCellType!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        self.selectionStyle = .none
    }
    
    func setupUI(){
        button.backgroundColor = .blue
        backgroundColor = .clear
    }
    
    func setupCell(type: FormCellType){
        button.setTitleColor(.white, for: .normal)
        self.type = type
        switch type {
        case .saveButton: button.setTitle(R.string.localizable.save_button(), for: .normal)
        default: button.setTitle(R.string.localizable.edit_button(), for: .normal)
        }
    }
    
    override func prepareForReuse() {
        buttonIsClicked = nil
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        buttonIsClicked?(type)
    }
}
