//
//  TextFieldCell.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Shared

public class TextFieldCell: UITableViewCell {
    @IBOutlet weak var fieldValue: UITextField!
    @IBOutlet weak var title: UILabel!
    var disposeBag: DisposeBag = DisposeBag()
    var onTextEdit: ((_ text: String) -> Void)?
    var type: FormCellType!
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        self.selectionStyle = .none
    }
    
    func setupUI(){
        backgroundColor = .clear
    }
    
    func setupCell(type: FormCellType, value: String?){
        self.type = type
        self.title.text = type.getString()
        self.fieldValue.text = value
        }
    
    override public func prepareForReuse() {
        fieldValue.text = String.empty
        disposeBag = DisposeBag()
    }
    
    public func setupPublisher(){
        fieldValue.rx.text.orEmpty
            .debounce(RxTimeInterval.milliseconds(3), scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .distinctUntilChanged()
            .enumerated()
            .skipWhile({ $0.index == 0 })
            .observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .background))
            .subscribe(onNext: {[unowned self] (string) in
                self.onTextEdit?(string.element)
                }, onError: { (error) in
                    debugPrint(error)
            }).disposed(by: disposeBag)
    }
}
