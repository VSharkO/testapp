//
//  PlayerFormCoordinator.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import Shared
import BaseKit

public class PlayerFormCoordinator: NSObject, Coordinator, CoordinatorDelegate{
    public weak var parent: ParentCoordinatorDelegate?
    
    public var childCoordinators: [Coordinator] = []
    public var presenter: UINavigationController
    let viewModel: PlayerFormViewModel
    let controller: PlayerFormViewController
    
    public init(presenter: UINavigationController, playerId: Int? = nil){
        self.presenter = presenter
        viewModel = PlayerFormViewModel(dependencies: PlayerFormViewModel.Dependecies(playerId: playerId, dbHelper: DbHelper()))
        controller = PlayerFormViewController(viewModel: viewModel)
    }
    
    deinit {
        printDeinit()
    }
    
    public func start() {
        controller.coordinatorDelegate = self
        presenter.pushViewController(controller, animated: true)
    }
    
    public func viewControllerHasFinished() {
        parent?.childHasFinished(coordinator: self)
    }
    
}

