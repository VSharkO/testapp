//
//  PlayerFormViewModel.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import BaseKit
import RxSwift
import Shared

public class PlayerFormViewModel: ViewModelType{
    
    public struct Dependecies{
        var playerId: Int?
        var dbHelper: DbHelper
    }
    
    public struct Input{
        let loadData: ReplaySubject<()>
        let userInteractionPublisher: PublishSubject<(index: Int, data: Any)>
        let pickerValueSelected: PublishSubject<(tableDataItem: Int, pickerItem: Int)>
        let pickerCellClicked: PublishSubject<Int>
        let saveButtonClicked: PublishSubject<()>
    }
    
    public struct Output: TableRefreshViewModelProtocol{
        public var refreshView: PublishSubject<TableRefresh>
        var tableData: [TableItem<FormCellType, Any?>]
        var countries: [Country]
        var clubs: [Club]
        var dataDisposable: Disposable
    }
    
    public func transform(input: PlayerFormViewModel.Input) -> PlayerFormViewModel.Output {
        let output = Output(refreshView: PublishSubject<TableRefresh>(), tableData: [], countries: [], clubs: [], dataDisposable: createDataDisposable(publisher: input.loadData))
        self.output = output
        return output
    }
    
    var dependencies: Dependecies
    var output: Output!
    var input: Input!
    public init(dependencies: Dependecies) {
        self.dependencies = dependencies
    }
    
}

//MARK: Data manipulation
public extension PlayerFormViewModel{
    
    func createDataDisposable(publisher: ReplaySubject<()>) -> Disposable{
        return publisher.flatMap({[unowned self] _ -> Observable<[TableItem<FormCellType,Any?>]> in
            return Observable.combineLatest(self.dependencies.dbHelper.getPlayersFromDb(),self.dependencies.dbHelper.getRegistrationsFromDb()).flatMap({[unowned self] (data) -> Observable<[TableItem<FormCellType,Any?>]> in
                var returnData: [TableItem<FormCellType,Any?>] = []
                if let id = self.dependencies.playerId{
                    let player = self.getPlayer(with: id, from: data.0)
                    let registration = self.getRegistration(with: id, from: data.1)
                    returnData = self.createItemData(from: PlayerFormData(player: player, registration: registration))
                }else{
                    returnData = self.createItemData(from: nil)
                }
                return Observable.just(returnData)
            })
        }).observeOn(MainScheduler.instance)
            .subscribeOn(ConcurrentDispatchQueueScheduler.init(qos: .background))
            .subscribe(onNext: {[unowned self] (data) in
                self.output.tableData = data
                self.output.refreshView.onNext(TableRefresh.complete)
                }, onError: {(error) in
                    debugPrint(error)
            })
    }
    
    func getPlayer(with id: Int, from players: [Player]) -> Player?{
        return players.first(where: { (pl) -> Bool in
            return pl.ID == id
        }) ?? nil
    }
    
    func getRegistration(with id: Int, from registrations: [PlayerRegistration]) -> PlayerRegistration?{
        return registrations.first(where: { (rg) -> Bool in
            return rg.playerId == id
        }) ?? nil
    }
    
    func createItemData(from data: PlayerFormData?) -> [TableItem<FormCellType,Any?>]{
        var innerData = [TableItem<FormCellType,Any?>]()
        innerData.append(TableItem.init(type: FormCellType.playerTitle, data: R.string.localizable.general_data()))
        if self.dependencies.playerId != nil{
            innerData.append(TableItem.init(type: FormCellType.id, data: data?.player?.ID))
        }
        innerData.append(TableItem.init(type: FormCellType.uniqueId, data: data?.player?.uniqueId))
        innerData.append(TableItem.init(type: FormCellType.firstName, data: data?.player?.firstName))
        innerData.append(TableItem.init(type: FormCellType.lastName, data: data?.player?.lastName))
        innerData.append(TableItem.init(type: FormCellType.dateOfBirth, data: data?.player?.dateOfBirth))
        innerData.append(TableItem.init(type: FormCellType.placeOfBirth, data: data?.player?.placeOfBirth))
        innerData.append(TableItem.init(type: FormCellType.country, data: getCountryByID(id: data?.player?.countryId)))
        innerData.append(TableItem.init(type: FormCellType.registrationTitle, data: R.string.localizable.registration_data()))
        innerData.append(TableItem.init(type: FormCellType.club, data:getClubByID(id: data?.registration?.clubId)))
        innerData.append(TableItem.init(type: FormCellType.dateFrom, data:data?.registration?.dateFrom))
        innerData.append(TableItem.init(type: FormCellType.dateTo, data:data?.registration?.dateTo))
        if self.dependencies.playerId != nil{
            innerData.append(TableItem.init(type: FormCellType.editButton, data: nil))
        }else{
            innerData.append(TableItem.init(type: FormCellType.saveButton, data: nil))
        }
        return innerData
    }
    
    func getCountryByID(id: Int?) -> Country?{
        if let id = id{
            return self.output.countries.first(where: {$0.ID == id}) ?? nil
        }else{
            return nil
        }
    }
    
    func getClubByID(id: Int?) -> Club?{
        if let id = id{
            return self.output.clubs.first(where: {$0.ID == id}) ?? nil
        }else{
            return nil
        }
    }
    
}
