//
//  FormCellType.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import Shared
public enum FormCellType{
    case playerTitle
    case id
    case uniqueId
    case firstName
    case lastName
    case dateOfBirth
    case placeOfBirth
    case country
    case registrationTitle
    case club
    case dateFrom
    case dateTo
    case saveButton
    case editButton
    
    func getString() -> String{
        switch self {
        case .playerTitle:
            return R.string.localizable.general_data()
        case .id:
            return R.string.localizable.unique_id()
        case .firstName:
            return R.string.localizable.first_name()
        case .dateOfBirth:
            return R.string.localizable.date_of_birth()
        case .placeOfBirth:
            return R.string.localizable.place_of_birth()
        case .country:
            return R.string.localizable.country()
        case .registrationTitle:
            return R.string.localizable.registration_data()
        case .club:
            return R.string.localizable.club()
        case .dateFrom:
            return R.string.localizable.date_from()
        case .dateTo:
            return R.string.localizable.date_to()
        case .saveButton:
            return R.string.localizable.save_button()
        case .editButton:
            return R.string.localizable.edit_button()
        @unknown default:
            return String.empty
        }
    }
}
