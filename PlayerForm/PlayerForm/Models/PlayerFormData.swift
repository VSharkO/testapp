//
//  PlayerFormData.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
import Shared

public struct PlayerFormData {
    public var player: Player?
    public var registration: PlayerRegistration?
}
