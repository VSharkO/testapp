//
//  PlayerFormViewController.swift
//  PlayerForm
//
//  Created by Valentin Šarić on 24/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
import Shared
import BaseUIKit
import BaseKit
import RxSwift

public class PlayerFormViewController: UIViewController, TableRefreshView{

    public typealias ItemRefreshable = UITableView
    
    public var itemRefreshable: UITableView {
        return tableView
    }
    @IBOutlet weak var tableView: UITableView!
    public weak var coordinatorDelegate: CoordinatorDelegate?
    public var viewModel: PlayerFormViewModel
    public var selectedTableItem = -1
    public var isKeyboardShown = false
    public var disposeBag = DisposeBag()
    let toolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.backgroundColor = .white
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(onToolbarCancelButtonTapped))
        cancelButton.tintColor = .black
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onToolbarDoneButtonTapped))
        doneButton.tintColor = .black
        let leadingSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        leadingSpaceButton.width = 15
        let trailingSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        trailingSpaceButton.width = 15
        
        toolbar.items = [leadingSpaceButton,
                         cancelButton,
                         UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                         doneButton,
                         trailingSpaceButton
        ]
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        return toolbar
    }()
    
    let pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.backgroundColor = .white
        pickerView.tintColor = .black
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }()
    
    init(viewModel: PlayerFormViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "\(PlayerFormViewController.self)", bundle: Bundle(for: PlayerFormViewController.self))
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("Displaying \(PlayerFormViewController.self)")
        registerCells()
        tableView.dataSource = self
        tableView.delegate = self
        pickerView.dataSource = self
        pickerView.delegate = self
        initViewModel()
    }
    
    let input = PlayerFormViewModel.Input.init(loadData: ReplaySubject<()>.create(bufferSize: 1), userInteractionPublisher: PublishSubject<(index: Int, data: Any)>(), pickerValueSelected: PublishSubject<(tableDataItem: Int, pickerItem: Int)>(), pickerCellClicked: PublishSubject<Int>(), saveButtonClicked: PublishSubject<()>())
    
    func initViewModel(){
        viewModel.input = self.input
        let output = viewModel.transform(input: self.input)
        initializeRefreshDriver(refreshObservable: output.refreshView)
        output.dataDisposable.disposed(by: disposeBag)
        input.loadData.onNext(())
  
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent{
            coordinatorDelegate?.viewControllerHasFinished()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        printDeinit()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func registerCells(){
         tableView.register(UINib(nibName: TextFieldCell.identifier, bundle: Bundle.init(for: TextFieldCell.self)), forCellReuseIdentifier: TextFieldCell.identifier)
    }
}

extension PlayerFormViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let item = viewModel.output.tableData[selectedTableItem]
        switch item.type{
        case .country:
            return viewModel.output.countries.count
        case .club:
            return viewModel.output.clubs.count
        default:
            debugPrint("data for picker is not handeled in viewModel")
            return 0
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item = viewModel.output.tableData[selectedTableItem]
        switch item.type{
        case .country:
            return viewModel.output.countries[row].name
        case .club:
            return viewModel.output.clubs[row].name
        default:
            debugPrint("data for picker is not handeled in viewModel")
            return .empty
        }
    }
    
    private func showPickerView(){
        guard !isKeyboardShown else{return}
        self.pickerView.reloadAllComponents()
        self.tableView.isUserInteractionEnabled = false
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
            guard let picker = self?.pickerView,
                let toolbar = self?.toolbar else{
                    return
            }
            
            self?.view.addSubview(picker)
            self?.view.addSubview(toolbar)
            
            picker.snp.makeConstraints { (maker) in
                maker.bottom.leading.trailing.equalToSuperview()
            }
            
            self?.toolbar.snp.makeConstraints { (maker) in
                maker.bottom.equalTo(picker.snp.top)
                maker.leading.trailing.equalToSuperview()
                maker.height.equalTo(50)
            }
            }, completion: nil)
    }
    
    fileprivate func closePickerView() {
        UIView.transition(with: view, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.toolbar.removeFromSuperview()
            self?.pickerView.removeFromSuperview()
        })
        self.tableView.isUserInteractionEnabled = true
    }
    
    @objc private func onToolbarDoneButtonTapped(){
        input.pickerValueSelected.onNext((tableDataItem: selectedTableItem ,pickerItem: pickerView.selectedRow(inComponent: 0)))
        closePickerView()
    }
    
    @objc private func onToolbarCancelButtonTapped(){
        closePickerView()
    }
    
    @objc func keyboardWillShow(notificaton: Notification) {
        isKeyboardShown = true
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        isKeyboardShown = false
    }
    
}

extension PlayerFormViewController: UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.tableData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let item = viewModel.output.tableData[indexPath.row]
        switch viewModel.output.tableData[indexPath.row].type {
        case .id:
            cell = setupTextCell(on: indexPath, with: item)
        default:
            cell = setupTextCell(on: indexPath, with: item)
        }
        return cell
    }
    
    func setupTextCell(on indexPath: IndexPath, with item: TableItem<FormCellType, Any?>) -> UITableViewCell{
        var cell = UITableViewCell()
        guard let data = item.data as? String else{return cell}
        cell = tableView.dequeueReusableCell(withIdentifier: TextFieldCell.identifier, for: indexPath)
        if let textCell = cell as? TextFieldCell{
            textCell.setupCell(type: item.type, value: data)
            textCell.onTextEdit = { [unowned self] text in self.input.userInteractionPublisher.onNext((index: indexPath.row, data: text))
                }
            textCell.setupPublisher()
            return textCell
        }else{
            return cell
        }
    }
    
}
