//
//  BaseUIKit.h
//  BaseUIKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BaseUIKit.
FOUNDATION_EXPORT double BaseUIKitVersionNumber;

//! Project version string for BaseUIKit.
FOUNDATION_EXPORT const unsigned char BaseUIKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BaseUIKit/PublicHeader.h>


