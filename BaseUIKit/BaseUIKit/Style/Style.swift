//
//  Style.swift
//  BaseUIKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
public struct Style{
    public let textColor: UIColor
    public let font: UIFont
    
    public init(textColor:UIColor, font: UIFont){
        self.textColor = textColor
        self.font = font
    }
}
