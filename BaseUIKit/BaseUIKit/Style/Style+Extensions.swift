//
//  Style+Extensions.swift
//  BaseUIKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import UIKit
extension UILabel: Stylable {
    
    public func applyStyle(_ style: Style){
        self.textColor = style.textColor
        self.font = style.font
    }
}
extension UITextView: Stylable {
    
    public func  applyStyle(_ style: Style){
        self.textColor = style.textColor
        self.font = style.font
    }
}
extension UITextField: Stylable {
    
    public func  applyStyle(_ style: Style){
        self.textColor = style.textColor
        self.font = style.font
    }
}
extension UIButton: Stylable {
    
    public func applyStyle(_ style: Style){
        self.setTitleColor(style.textColor, for: .normal)
        self.titleLabel?.font = style.font
    }
}
