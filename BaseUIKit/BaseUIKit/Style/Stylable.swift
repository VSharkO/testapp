//
//  Stylable.swift
//  BaseUIKit
//
//  Created by Valentin Šarić on 22/06/2019.
//  Copyright © 2019 Valentin Šarić. All rights reserved.
//

import Foundation
public protocol Stylable: class {
    func applyStyle(_ style: Style)
}

